## 1.0.1

* Added new annotations (SfwConfig and SfwImportLibraryConfig) for [sfw_generator](https://pub.dev/packages/sfw_generator)
