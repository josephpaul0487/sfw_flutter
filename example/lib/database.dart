import 'package:sfw_imports/sfw_imports.dart';
import 'models.dart';

@SfwDbConfig('ops', version: 2)
abstract class DB {
  @SfwDbQuery(UserModel, false, 'tbl_user', limit: 1, orderBy: "updated_at")
  getUser();
  static onCreate(db, version) {}

  static onUpgrade(db, version, newVersion) {}

  static onDowngrade(db, version, newVersion) async {}

  static onOpen(db) {}
}
