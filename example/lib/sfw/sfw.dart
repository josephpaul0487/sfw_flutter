/// COMMON
const DEBUGGING_ENABLED = false;
const TOTAL_DART_FILE_COUNT = 10; //Total dart file count of your project

/// DATABASE CONFIGURATION
const DB_DART_FILE_PATH = "database.dart"; //eg: database.dart
const DB_VERSION_CODE = 1;

/// WEB CONFIGURATION
const WEB_DART_FILE_PATH = "web.dart"; //eg: web.dart
const WEB_BASE_URL = "https://www.google.com/"; //eg: web.dart

/// IMPORT LIBRARY CONFIGURATION
const bool INCLUDE_CACHED_NETWORK_IMAGE = true;
const bool INCLUDE_FLUTTER_DATE_TIME_PICKER = true;
const bool INCLUDE_PERMISSION_HANDLER = true;
const bool INCLUDE_FLUTTER_SPIN_KIT = true;
const bool INCLUDE_FLUTTER_TOAST = false;
const bool INCLUDE_DIO = true;
