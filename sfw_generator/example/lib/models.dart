import 'package:sfw_imports/sfw_imports.dart'
    show SfwEntity, HelperMethods, SfwDbField, SfwDbPrimary;

@SfwEntity(["tbl_user"],needTableMethods:true)
class UserModel with HelperMethods {
  @SfwDbPrimary(true)
  String id;
  String email;
  String firstName;
  String lastName;
  String countryCode;
  String mobile;
  String token;
  String image;
  @SfwDbField("created_at")
  String createdAt;
  @SfwDbField("updated_at")
  String updatedAt;

  bool keepSignedIn;

  @SfwDbField("tests",genericType: ListTest,isAnEntity: true)
  List<ListTest> tests;

  UserModel(
      {this.id,
        this.tests,
      this.email,
      this.firstName,
      this.lastName,
      this.countryCode,
      this.mobile,
      this.token,
      this.image,
      this.createdAt,
      this.updatedAt,
      this.keepSignedIn});

  getEmail() => avoidNull(email);

  getName() => "${avoidNull(firstName)} ${avoidNull(lastName)}";

  getImage() => avoidNull(image);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserModel && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

@SfwEntity(["test"])
class ListTest {
  @SfwDbPrimary(true)
  int id;

  ListTest({this.id});

}
