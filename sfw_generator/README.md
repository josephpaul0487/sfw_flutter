# sfw_generator

database generator

## Not supporting foreign keys and other complicated features

## Codes to generate files

### Step 1:  flutter packages pub run build_runner clean
### Step 2:  flutter packages pub run build_runner build      OR
###          flutter packages pub run build_runner build --delete-conflicting-outputs


## Files

    * Create a new directory under lib folder named "sfw" => without quotes
    * Create following dart files under sfw folder
      1. animations.dart
      2. sfw.dart
      3. strings.dart
      4. styles.dart
      5. ui.dart
      6. ui_helper.dart
      7. colors.xml
      8. constants.xml
      9. String files

## sfw.dart

    ### Add following details to sfw.dart file
    
    COMMON

    - const DEBUGGING_ENABLED = false;
    - const TOTAL_DART_FILE_COUNT = 1; //Total dart file count of your project

    DATABASE CONFIGURATION

    - const DB_DART_FILE_PATH = "";//eg: lib/database.dart
    - const DB_VERSION_CODE = 1;

    WEB CONFIGURATION
    
    - const WEB_DART_FILE_PATH = "web.dart";//eg: lib/web.dart
    - const WEB_BASE_URL = "";//eg: https://www.google.com/

    IMPORT LIBRARY CONFIGURATION
    
    - const bool INCLUDE_CACHED_NETWORK_IMAGE = true;
    - const bool INCLUDE_FLUTTER_DATE_TIME_PICKER = true;
    - const bool INCLUDE_PERMISSION_HANDLER = true;
    - const bool INCLUDE_FLUTTER_SPIN_KIT = true;
    - const bool INCLUDE_FLUTTER_TOAST = true;
    - const bool INCLUDE_DIO = true


For more info  see [Sfw Flutter] (https://bitbucket.org/josephpaul0487/sfw_flutter)

THANKS
-

## [flutter_screenutil](https://pub.dev/packages/flutter_screenutil)
## [json_serializable](https://pub.dev/packages/json_serializable)
