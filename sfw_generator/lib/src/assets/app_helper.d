import 'package:flutter/services.dart' show SystemChannels;
import 'styles.sfw.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
TOAST_IMPORT_START
import 'package:fluttertoast/fluttertoast.dart';
import 'ui_helper.sfw.dart';
TOAST_IMPORT_END
import 'package:intl/intl.dart' show DateFormat;
PERMISSION_IMPORT_START
import 'package:permission_handler/permission_handler.dart';
PERMISSION_IMPORT_END


typedef SfwNotifierBuilder=Widget Function(BuildContext context,String key, dynamic data);

abstract class SfwNotifierListener {
  Future<dynamic> onSfwNotifierCalled(String key, dynamic data);
}

class SfwNotifier {
  final Map<SfwNotifierListener, Set<String>> _listeners = {};
  static SfwNotifier _notifier = SfwNotifier();


  static addListener(SfwNotifierListener listener, Set<String> keys,{SfwNotifier instance}) {
    if (listener == null)
      return;
    Set<String> list = (instance ?? _notifier)._listeners[listener];
    if (list == null) {
      list = {};
    }
    if (keys != null)
      list.addAll(keys);
    (instance ?? _notifier)._listeners[listener] = list;
  }

  static removeListener(SfwNotifierListener listener,{SfwNotifier instance}) {
    if (listener == null)
      return;
    (instance ?? _notifier)._listeners.remove(listener);
  }

  static Future<Map<Type,dynamic>> notify(String key, dynamic value,{SfwNotifier instance,bool useAsync = false}) async {
    if (key == null || key
        .trim()
        .isEmpty) {
      return {};
    }
    List<MapEntry<SfwNotifierListener,Set<String>>> list = (instance ?? _notifier)?._listeners?.entries?.toList();
    Map<Type,dynamic> data = {};
    for(int i=0;i<(list.length ?? 0);++i) {
      final entry = list[i];
      if(entry.value?.contains(key) ?? false) {
        if(useAsync) {
          data[entry.key.runtimeType] = await entry.key?.onSfwNotifierCalled(key, value);
        } else {
          entry.key?.onSfwNotifierCalled(key, value);
        }

      }
    }
    return data;
    // (instance ?? _notifier)._listeners.forEach((listener, _keys) {
    //   if (_keys?.contains(key) ?? false) {
    //     listener?.onSfwNotifierCalled(key, value);
    //   }
    // });
  }
}

class SfwNotifierForSingleKey {
  final Map<String, List<SfwNotifierListener>> _listeners = {};
  static SfwNotifierForSingleKey _notifier = SfwNotifierForSingleKey();

  static addListener(SfwNotifierListener listener, String key,{SfwNotifierForSingleKey instance}) {
    if (listener == null || key == null || key
        .trim()
        .isEmpty)
      return;
    List<SfwNotifierListener> list = (instance ?? _notifier)._listeners[key];

    if (list == null) {
      list = [];
    }
    if(!list.contains(listener)) {
      list.add(listener);
      (instance ?? _notifier)._listeners[key] = list;
    }


  }

  static removeListener(SfwNotifierListener listener, String key,{SfwNotifierForSingleKey instance}) {
    if (listener == null || key == null || key
        .trim()
        .isEmpty)
      return;
    List<SfwNotifierListener> list = (instance ?? _notifier)._listeners[key];
    if (list == null) {
      return;
    }
    list.remove(listener);
    if (list.isEmpty) {
      (instance ?? _notifier)._listeners.remove(key);
    } else {
      (instance ?? _notifier)._listeners[key] = list;
    }
  }

  static Future<Map<Type,dynamic>> notify(String key, dynamic value,{SfwNotifierForSingleKey instance,bool useAsync : false}) async {
    if (key == null || key
        .trim()
        .isEmpty) {
      return {};
    }
    Map<Type,dynamic> data = {};
    if(useAsync) {
      try {
        final list = (instance ?? _notifier)._listeners[key] ?? [];
        if(list!=null) {
          for(int i=0;i<list.length;++i) {
            final listener = list[i];
            if(listener!=null) {
              data[listener.runtimeType] = await listener?.onSfwNotifierCalled(key, value);
            }
          }
        }
      } catch(e) {}

    } else {
      final list = (instance ?? _notifier)._listeners[key] ?? [];
      for(int i=0;i<list.length;++i) {
        final listener = list[i];
        listener?.onSfwNotifierCalled(key, value);
      }
      // (instance ?? _notifier)._listeners[key]?.forEach((listener) {
      //   listener?.onSfwNotifierCalled(key, value);
      // });
    }
    return data;

  }
}


class SfwNotifierSingleKeyWidget extends StatefulWidget {
  final SfwNotifierBuilder builder;
  final String notifierKey;
  final dynamic initialData;

  const SfwNotifierSingleKeyWidget(
      {Key key, @required this.builder, @required this.notifierKey, this.initialData})
      : assert(builder != null),
        assert(notifierKey != null && notifierKey != ""),
        super(key: key);

  @override
  _SfwNotifierWidgetState createState() => _SfwNotifierWidgetState();
}

class _SfwNotifierWidgetState extends SfwState<SfwNotifierSingleKeyWidget>
    implements SfwNotifierListener {

  dynamic data;


  @override
  void initState() {
    data = widget.initialData;
    SfwNotifierForSingleKey.addListener(this, widget.notifierKey);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget w= widget.builder(context,widget.notifierKey, data);
    this.data=widget.initialData;//to avoid holding data -> it may cause error in ListView
    return w;
  }

  @override
  Future<dynamic> onSfwNotifierCalled(String key, data) {
    setState(() {
      this.data = data;
    });
    return null;
  }

  @override
  void dispose() {
    SfwNotifierForSingleKey.removeListener(this, widget.notifierKey);
    super.dispose();
  }


}

class SfwNotifierMultiKeyWidget extends StatefulWidget {
  final SfwNotifierBuilder builder;
  final Set<String> notifierKeys;
  final dynamic initialData;
  final String initialKey;

  const SfwNotifierMultiKeyWidget(
      {Key key, @required this.builder, @required this.notifierKeys, this.initialData, this.initialKey})
      : assert(builder != null),
        assert(notifierKeys != null),
        super(key: key);

  @override
  _SfwNotifierMultiKeyWidgetState createState() =>
      _SfwNotifierMultiKeyWidgetState();
}

class _SfwNotifierMultiKeyWidgetState extends SfwState<SfwNotifierMultiKeyWidget>
    implements SfwNotifierListener {

  dynamic data;
  String key;

  @override
  void initState() {
    data = widget.initialData;
    SfwNotifier.addListener(this, widget.notifierKeys);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget w= widget.builder(context,key, data);
    this.key=null;
    this.data=widget.initialData;//to avoid holding data -> it may cause error in ListView
    return w;
  }

  @override
  Future<dynamic> onSfwNotifierCalled(String key, data) {
    setState(() {
      this.key = key;
      this.data = data;
    });
    return null;
  }

  @override
  void dispose() {
    SfwNotifier.removeListener(this);
    super.dispose();
  }


}


class SfwHelper {
  static ScreenUtil util;
  static double screenWidth;
  static double screenWidthPx;
  static double screenHeight;
  static double screenHeightPx;
  static bool isVertical = true;
  static const String yyyy_MM_dd_HH_mm_ss_S_Z = "yyyy-MM-dd HH:mm:ss.SZ";
  static const String MM_dd_yyyy_KK_mm_ss__a = "MM-dd-yyyy KK:mm:ss a";
  static const String MM_dd_yyyy_KK_mm_ss_a = "MM-dd-yyyy KK:mm:ssa";


  static initialize(BuildContext context,{double width = 1080,
    double  height = 1920,
    bool allowFontScaling = false,BorderRadius edtBorderRadius}) {
    if (util == null || util.scaleWidth < 1) {
      ScreenUtil.init(context,width: width,height: height,allowFontScaling: allowFontScaling);
      TIL.edtBorderRadius = edtBorderRadius ?? BorderRadius.all(Radius.circular(SfwConstants.instance().edtBoarderRadius));

      util = ScreenUtil();
      screenHeight = ScreenUtil.screenHeight;
      screenHeightPx = ScreenUtil.screenHeightPx;
      screenWidth = ScreenUtil.screenWidth;
      screenWidthPx = ScreenUtil.screenWidthPx;
      isVertical = screenWidth < screenHeight;
    }
  }

  static setWidth(double widthInPx) => util.setWidth(widthInPx);

  static setHeight(double heightInPx) => util.setHeight(heightInPx);

  static setWidthDpToPx(double widthInDp) => widthInDp / (util.scaleWidth==0?1:util.scaleWidth);

  static setHeightDpToPx(double heightInDp) => heightInDp / (util.scaleHeight==0?1:util.scaleHeight);

  static setSp(double fontSize) => util.setSp(fontSize);

  static double pxToDp(double px) {
    return isVertical ? setWidth(px) : setHeight(px);
  }

  static double dpToPx(double dp) {
    return isVertical ? setWidthDpToPx(dp) : setHeightDpToPx(dp);
  }



  static String numberToString(int number,
      {int minLength = 2, bool isForPrefix = true, String deliminator = "0"}) {
    if (number == null || minLength == null || isForPrefix == null) {
      return number == null ? "" : "$number";
    }
    String num = "$number";
    if (num.length == minLength) {
      return "$number";
    }
    for (int i = num.length; i < minLength; ++i) {
      if (isForPrefix)
        num = deliminator + num;
      else
        num += deliminator;
    }
    return num;
  }

  static String convertDate(String fromFormat, String toFormat, String date) {
    try {
      return DateFormat(toFormat).format(DateFormat(fromFormat).parse(date));
    } on FormatException {
      return "";
    }
  }

  static String convertDateTime(String toFormat, DateTime date) {
    try {
      return DateFormat(toFormat).format(date);
    } on FormatException {
      return "";
    }
  }

  static String convertWebToLocalDate(String toFormat, String date,
      {String fromFormat = "yyyy-MM-dd HH:mm:ss"}) {
    try {
      return DateFormat(toFormat).format(DateFormat(fromFormat).parse(date));
    } on FormatException {
      return "";
    }
  }

  static String convertUtcToLocalDate(String fromFormat, String toFormat,
      String date) {
    try {
      return DateFormat(toFormat).format(DateFormat(fromFormat).parse(date).toLocal());
    } on FormatException {
      return "";
    }
  }

  static String convertLocalToUtcDate(String fromFormat, String toFormat,
      String date) {
    try {
      return DateFormat(toFormat).format(DateFormat(fromFormat).parse(date).toUtc());
    } catch (e) {
      return "";
    }
  }

  static String convertLocalToWebDate(String fromFormat, String date,
      {String toFormat = "yyyy-MM-dd HH:mm:ss"}) {
    return convertLocalToUtcDate(fromFormat, toFormat, date);
  }

  static bool isDateAfter(String format, String date, String dateToCompare) {
    try {
      return DateFormat(format).parse(date).isAfter(
          DateFormat(format).parse(dateToCompare));
    } catch (e) {
      return false;
    }
  }

  static bool isDateBefore(String format, String date, String dateToCompare) {
    try {
      return DateFormat(format).parse(date).isBefore(
          DateFormat(format).parse(dateToCompare));
    } catch (e) {
      return false;
    }
  }

  static bool isEqual(String format, String date, String dateToCompare) {
    try {
      return DateFormat(format).parse(date).isAtSameMomentAs(
          DateFormat(format).parse(dateToCompare));
    } catch (e) {
      return false;
    }
  }

  static bool isDateAfterDateTime(String format, String date,
      DateTime dateToCompare) {
    try {
      return DateFormat(format).parse(date).isAfter(dateToCompare);
    } catch (e) {
      return false;
    }
  }

  static bool isDateBeforeDateTime(String format, String date,
      DateTime dateToCompare) {
    try {
      return DateFormat(format).parse(date).isBefore(dateToCompare);
    } catch (e) {
      return false;
    }
  }

  static bool isEqualDateTime(String format, String date,
      DateTime dateToCompare) {
    try {
      return DateFormat(format).parse(date).isAtSameMomentAs(dateToCompare);
    } catch (e) {
      return false;
    }
  }

  static DateTime parse(String format, String date,
      {DateTime defDateTime, bool isDateInUtc = false}) {
    try {
      DateTime dateTime = DateFormat(format).parse(date);
      if(isDateInUtc) {
        return dateTime.toLocal();
      }
      return dateTime;
    } catch (e) {
      return defDateTime ?? null;
    }
  }

  static void pop() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  static void hideKeyboard(BuildContext ctx) {
    try {
      FocusScope.of(ctx).requestFocus(FocusNode());
      // SystemChannels.textInput.invokeMethod('TextInput.hide');
    } catch (e) {}
  }

  static String extension(String fileUrl) {
    return fileUrl == null || !fileUrl.contains(".") ? "" : fileUrl.substring(
        fileUrl.indexOf(".") + 1);;
  }

  static bool isDocument(fileUrl) {
    String ext = extension(fileUrl).toLowerCase();
    return ext != "" &&
        (ext == 'pdf' || ext == 'doc' || ext == 'txt' || ext == 'docx' ||
            ext == 'ppt' || ext == 'pptx' || ext == 'xls' || ext == 'xlsx');
  }

  static bool isImage(fileUrl) {
    String ext = extension(fileUrl).toLowerCase();
    return ext != "" && (ext == 'jpg' || ext == 'jpeg' || ext == 'png');
  }

  PERMISSION_STARTED
  static Future<bool> hasPermission(Permission permission,
      [String toastMessage]) async {

    try {
      PermissionStatus hasPermission =
      await permission.status;
      if (hasPermission == null || hasPermission != PermissionStatus.granted) {
        Map<Permission, PermissionStatus> permissions =
        await [permission].request();
        if (permissions == null ||
            permissions.length == 0 ||
            !permissions.containsKey(permission) ||
            permissions[permission] != PermissionStatus.granted) {
          TOAST_6_STARTED
          if (toastMessage != null && toastMessage.isNotEmpty)
            showToast(toastMessage);
          TOAST_6_END
          return false;
        }
      }
    } catch (e) {
      return false;
    }
    return true;
  }

  static Future<bool> hasPermissions(List<Permission> permissions,
      [List<String> toastMessages]) async {
    if(permissions==null)
      return false;
    if(permissions.isEmpty)
      return true;
    bool isToastEmpty = toastMessages == null || toastMessages.isEmpty;
    if (!isToastEmpty && permissions.length != toastMessages.length) {
      throw Exception(
          "Either toastMessages is empty or toastMessages.length == permissions.length");
    }

    for (int i = 0; i < permissions.length; ++i) {
      if (!await hasPermission(
          permissions[i], isToastEmpty ? null : toastMessages[i])) {
        return false;
      }
    }
    return true;
  }

  PERMISSION_END

  TOAST_STARTED
  static void showToast(String msg,{Duration toastLength: const Duration(seconds: 1),
    gravity: ToastGravity.CENTER,
    Color textColor: Colors.white,Color backgroundColor= Colors.black,double fontSize,Widget child,BorderRadius borderRadius,BuildContext context}) {
    try {
      if(context !=null) {
        FToast(context).showToast(child: child ?? Container(decoration:BoxDecoration(color: backgroundColor,shape: BoxShape.rectangle,borderRadius: borderRadius ?? BorderRadius.all(Radius.circular(20),),),padding: EdgeInsets.only(left: SfwHelper.setWidth(30),right: SfwHelper.setWidth(30),top: SfwHelper.setHeight(20),bottom: SfwHelper.setHeight(20),),child: SfwUiHelper.commonText(msg,textSize: fontSize==null? SfwHelper.setSp(30.0) : fontSize,textColor: textColor)),toastDuration: toastLength,
            gravity: gravity);
      } else {
        Fluttertoast.showToast(msg: msg,timeInSecForIosWeb: toastLength.inSeconds,fontSize: fontSize ?? SfwHelper.setSp(30.0) ,gravity: gravity,textColor: textColor,backgroundColor: backgroundColor,toastLength: toastLength.inSeconds>1?Toast.LENGTH_LONG:Toast.LENGTH_SHORT);
      }

    } catch (e) {
    }
  }
  TOAST_END

}

abstract class SfwState <T extends StatefulWidget> extends State<T> {
  bool  _isDestroyed = false;
  bool get isDestroyed => _isDestroyed;

  @override
  void setState([fn]) {
    try {
      if(mounted && !_isDestroyed)
        super.setState(fn ?? (){});
    } catch(e) {
    }
  }

  @override
  void initState() {
    _isDestroyed = false;
    super.initState();
    SfwHelper.hideKeyboard(context);
  }

  @override
  void dispose() {
    super.dispose();
    _isDestroyed = true;
  }
  TOAST_2
  bool toastMessage(bool showToast,String toastMessage) {
    if(showToast && (toastMessage!=null && toastMessage.isNotEmpty))
      SfwHelper.showToast(toastMessage,context:context);
    return showToast;
  }

  bool isEmpty(String str,[String toastMessage]) {
    return this.toastMessage(str==null || str.trim().isEmpty ,toastMessage);
  }
  TOAST_2_END




}

