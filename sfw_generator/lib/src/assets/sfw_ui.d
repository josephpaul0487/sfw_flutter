import 'dart:async';

import 'package:flutter/services.dart';
DATE_IMPORT_START
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
DATE_IMPORT_END
import 'strings.sfw.dart';
import 'styles.sfw.dart';
import 'package:sfw_imports/sfw_imports.dart' show KeyValueValidator,OnClickCallback2, Validator;
import 'package:flutter/material.dart';
import 'ui_helper.sfw.dart';
import 'sfw.sfw.dart';

abstract class DropdownItemDelegate {
  String getDropdownItemString();

}
class StaticConstants {
  static const String NULL_REPLACE_TEXT = '!@#%^&*()~`!!:<>';
}
typedef SfwTextChangeCallBack = Function(Function(VoidCallback) setter,TextEditingController controler,SfwTil til);

class ProgressButtonModel {
  ProgressButtonStates state;
  String text;
  OnClickCallback2 onPressed;
  int milliSecondsToNormal;
  int id;
  double elevation;
  TextStyle textStyle;

  final Color backgroundColor;
  final Color splashColor;
  final ShapeBorder shape;
  final Color borderColor;

  final double height;

  final double widthFactor;

  final EdgeInsetsGeometry padding;
  final Color progressColor;
  final Widget progressWidget;
  SfwIconData successWidget;
  final BorderRadius borderRadius;

  ProgressButtonModel(
      this.text,
      this.onPressed, {
        this.borderRadius,
        this.state = ProgressButtonStates.normal,
        this.milliSecondsToNormal = -1,
        this.id = 0,
        this.elevation = SfwConstants.btnElevation,
        this.textStyle,
        this.backgroundColor,
        this.shape,
        this.borderColor = SfwColors.dividerColor,
        this.splashColor = SfwColors.btnCommonSplashBack,
        this.successWidget,
        this.progressColor = SfwColors.accentColor,
        this.progressWidget,
        this.height,
        this.widthFactor = 1,
        this.padding,
      });

  String get listenerKey => "$listenerKeySuffix$id";

  static String get listenerKeySuffix => "ProgressButton";
}

class SfwIconData {
  final IconData icon;
  final double size;
  final Color tint;
  final Color focusTint;
  final EdgeInsets iconPadding;
  final Widget widget;
  final VoidCallback onPressed;

  const SfwIconData({this.icon,
    this.tint,
    this.focusTint,
    this.widget,
    this.iconPadding = const EdgeInsets.all(null),
    this.size,
    this.onPressed});

  SfwIconData copyWith(SfwIconData icon) {
    return SfwIconData(icon: icon.icon ?? this.icon,
        tint: icon.tint ?? this.tint,
        focusTint: icon.focusTint ?? this.focusTint,
        widget: icon.widget ?? this.widget,
        iconPadding: icon.iconPadding ?? this.iconPadding,
        onPressed: icon.onPressed ?? this.onPressed,
        size: icon.size ?? this.size);
  }
}

class SfwTil {
  final SfwTilIcons icons;
  final SfwTilStyles styles;
  final SfwTilText texts;
  final SfwTilDecoration decoration;
  final SfwTilBool booleans;
  final SfwTilActions actions;
  final SfwTilTextProperty textProperty;
  final SfwTilTextsRelated textsRelated;
  final SfwTilOthers others;
  final double height;
  final double width;
  final Alignment alignment;
  final bool isWHpercentage;
  final String notifierKey;
  final bool disposeUserDefinedController;
  final Key key;

  const SfwTil({this.icons = const SfwTilIcons(),
    this.styles = const SfwTilStyles(),
    this.texts = const SfwTilText(),
    this.decoration = const SfwTilDecoration(),
    this.booleans = const SfwTilBool(),
    this.actions = const SfwTilActions(),
    this.textProperty = const SfwTilTextProperty(),
    this.textsRelated = const SfwTilTextsRelated(),
    this.others = const SfwTilOthers(),
    this.key,
    this.height,
    this.width,
    this.alignment,
    this.isWHpercentage = false,
    this.notifierKey,
    this.disposeUserDefinedController = true}):assert(textProperty!=null),assert( textsRelated !=null),assert( others != null),assert( icons!=null),assert( styles!=null),assert( texts!=null),assert( decoration!=null),assert( booleans!=null),assert(actions!=null );

  SfwTil copyWith(SfwTil til) {
    return SfwTil(icons: til.icons ?? this.icons,
        notifierKey: til.notifierKey ?? this.notifierKey,
        actions: til.actions ?? this.actions,
        width: til.width ?? this.width,
        texts: til.texts ?? this.texts,
        booleans: til.booleans ?? this.booleans,
        decoration: til.decoration ?? this.decoration,
        textsRelated: til.textsRelated ?? this.textsRelated,
        textProperty: til.textProperty ?? this.textProperty,
        height: til.height ?? this.height,
        alignment: til.alignment ?? this.alignment,
        styles: til.styles ?? this.styles,
        disposeUserDefinedController: til.disposeUserDefinedController ??
            this.disposeUserDefinedController,
        isWHpercentage: til.isWHpercentage ?? this.isWHpercentage);
  }


}

class SfwTilStyles {
  final TextStyle style;
  final TextStyle helperStyle;
  final TextStyle labelStyle;
  final TextStyle hintStyle;
  final TextStyle errorStyle;
  final TextStyle prefixStyle;
  final TextStyle suffixStyle;
  final TextStyle counterStyle;

  final StrutStyle strutStyle;
  final TextAlign textAlign;

  const SfwTilStyles({
    this.style,
    this.strutStyle,
    this.textAlign = TextAlign.start,
    this.helperStyle,
    this.labelStyle,
    this.hintStyle,
    this.errorStyle,
    this.prefixStyle,
    this.suffixStyle,
    this.counterStyle,
  });

  SfwTilStyles copyWith(SfwTilStyles style) {
    return SfwTilStyles(counterStyle: style.counterStyle ?? this.counterStyle,
        errorStyle: style.errorStyle ?? this.errorStyle,
        helperStyle: style.helperStyle ?? this.helperStyle,
        hintStyle: style.hintStyle ?? this.hintStyle,
        labelStyle: style.labelStyle ?? this.labelStyle,
        prefixStyle: style.prefixStyle ?? this.prefixStyle,
        strutStyle: style.strutStyle ?? this.strutStyle,
        style: style.style ?? this.style,
        suffixStyle: style.suffixStyle ?? this.suffixStyle,
        textAlign: style.textAlign ?? this.textAlign);
  }
}

class SfwTilTextProperty {
  final TextInputType inputType;
  final TextInputAction inputAction;
  final TextDirection textDirection;

  final TextCapitalization textCapitalization;
  final Color cursorColor;
  final double cursorWidth;
  final Radius cursorRadius;
  final Brightness keyboardAppearance;

  final InputCounterWidgetBuilder buildCounter;
  final TextAlignVertical textAlignVertical;

  const SfwTilTextProperty({
    this.cursorColor,
    this.cursorWidth,
    this.cursorRadius,
    this.keyboardAppearance,
    this.buildCounter,
    this.inputType = TextInputType.text,
    this.inputAction = TextInputAction.next,
    this.textDirection,
    this.textCapitalization = TextCapitalization.none,
    this.textAlignVertical
  });

  SfwTilTextProperty copyWith(SfwTilTextProperty property) {
    return SfwTilTextProperty(inputType: property.inputType ?? this.inputType,
        inputAction: property.inputAction ?? this.inputAction,
        cursorColor: property.cursorColor ?? this.cursorColor,
        buildCounter: property.buildCounter ?? this.buildCounter,
        cursorRadius: property.cursorRadius ?? this.cursorRadius,
        cursorWidth: property.cursorWidth ?? this.cursorWidth,
        keyboardAppearance: property.keyboardAppearance ??
            this.keyboardAppearance,
        textCapitalization: property.textCapitalization ??
            this.textCapitalization,
        textDirection: property.textDirection ?? this.textDirection);
  }
}

class SfwTilText {
  final String labelText;
  final String helperText;
  final String hintText;
  final String errorText;
  final String passwordErrorText;
  final String text;
  final String counterText;
  final String suffixText;
  final String prefixText;
  final String semanticCounterText;

  const SfwTilText({
    this.labelText,
    this.helperText,
    this.hintText,
    this.errorText,
    this.text,
    this.counterText,
    this.suffixText,
    this.prefixText,
    this.semanticCounterText,
    this.passwordErrorText,
  });

  SfwTilText copyWith(SfwTilText text) {
    return SfwTilText(
      labelText: text.labelText == StaticConstants.NULL_REPLACE_TEXT? null: text.labelText ?? this.labelText,
      helperText: text.helperText== StaticConstants.NULL_REPLACE_TEXT? null: text.helperText?? this.helperText,
      hintText: text.hintText== StaticConstants.NULL_REPLACE_TEXT? null:text.hintText ?? this.hintText,
      errorText: text.errorText== StaticConstants.NULL_REPLACE_TEXT? null: text.errorText?? this.errorText,
      passwordErrorText: text.passwordErrorText== StaticConstants.NULL_REPLACE_TEXT? null: text.passwordErrorText?? this.passwordErrorText,
      text: text.text== StaticConstants.NULL_REPLACE_TEXT? null:  text.text?? this.text,
      counterText: text.counterText== StaticConstants.NULL_REPLACE_TEXT? null: text.counterText?? this.counterText,
      suffixText: text.suffixText== StaticConstants.NULL_REPLACE_TEXT? null: text.suffixText?? this.suffixText,
      prefixText: text.prefixText== StaticConstants.NULL_REPLACE_TEXT? null: text.prefixText?? this.prefixText,
      semanticCounterText: text.semanticCounterText== StaticConstants.NULL_REPLACE_TEXT? null: text.semanticCounterText??
          this.semanticCounterText,);
  }
}

class SfwTilTextsRelated {
  final int hintMaxLines;
  final int errorMaxLines;
  final int helperMaxLines;
  final int maxLength;
  final int minLength;
  final int maxLines;
  final int minLines;


  const SfwTilTextsRelated({this.hintMaxLines,
    this.errorMaxLines,
    this.helperMaxLines,
    this.maxLength = SfwConstants.edtMaxLength,
    this.minLength = 0,
    this.maxLines = 1,
    this.minLines});

  static SfwTilTextsRelated multiLine(SfwTilTextsRelated texts) =>
      SfwTilTextsRelated(
          hintMaxLines: texts.hintMaxLines,
          errorMaxLines: texts.errorMaxLines,
          minLength: texts.minLength ?? 0,
          maxLength: SfwConstants.edtMultiMaxLength,
          maxLines: SfwConstants.edtMultiMaxLines,
          minLines: SfwConstants.edtMultiMinLines);

  static SfwTilTextsRelated password(SfwTilTextsRelated texts) =>
      SfwTilTextsRelated(
          hintMaxLines: texts.hintMaxLines,
          errorMaxLines: texts.errorMaxLines,
          maxLength: SfwConstants.edtPasswordMaxLength,
          minLength: SfwConstants.edtPasswordMinLength);

  SfwTilTextsRelated copyWith(SfwTilTextsRelated related) =>
      SfwTilTextsRelated(
          hintMaxLines: related.hintMaxLines ?? this.hintMaxLines,
          errorMaxLines: related.errorMaxLines ?? this.errorMaxLines,
          maxLength: related.maxLength ?? this.maxLength,
          minLength: related.minLength ?? this.minLength,
          maxLines: related.maxLines ?? this.maxLines,
          minLines: related.minLines ?? this.minLines);
}

class SfwTilIcons {
  final SfwIconData passwordHiddenIcon;
  final SfwIconData passwordShowIcon;
  final SfwIconData prefixIcon;
  final SfwIconData suffixIcon;
  final SfwIconData icon;
  final Widget prefix;
  final Widget suffix;
  final Widget counter;

  const SfwTilIcons({
    this.passwordHiddenIcon = const SfwIconData(),
    this.passwordShowIcon = const SfwIconData(),
    this.prefixIcon = const SfwIconData(),
    this.suffixIcon = const SfwIconData(),
    this.icon = const SfwIconData(),
    this.prefix,
    this.suffix,
    this.counter,
  });

  SfwTilIcons copyWith(SfwTilIcons icons) {
    return SfwTilIcons(
        passwordHiddenIcon: icons.passwordHiddenIcon ?? this.passwordHiddenIcon,
        icon: icons.icon ?? this.icon,
        suffixIcon: icons.suffixIcon ?? this.icon,
        counter: icons.counter ?? this.counter,
        passwordShowIcon: icons.passwordShowIcon ?? this.passwordShowIcon,
        prefix: icons.prefix ?? this.prefix,
        prefixIcon: icons.prefixIcon ?? this.prefixIcon,
        suffix: icons.suffix ?? this.suffix);
  }
}

class SfwTilActions {
  final TextEditingController controller;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final FocusNode focusNode;
  final List<TextInputFormatter> inputFormatters;
  final VoidCallback onEditingComplete;
  final ValueChanged<String> onFieldSubmitted;
  final SfwTextChangeCallBack onTextChangeListener;
  final ValueChanged<String> onChanged;
  final VoidCallback onTap;

  const SfwTilActions({this.controller,
    this.onSaved,
    this.validator,
    this.focusNode,
    this.inputFormatters,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.onTextChangeListener,
    this.onChanged,
    this.onTap});

  SfwTilActions copyWith(SfwTilActions action) {
    return SfwTilActions(
        controller: action.controller ?? this.controller,
        onSaved: action.onSaved ?? this.onSaved,
        validator: action.validator ?? this.validator,
        focusNode: action.focusNode ?? this.focusNode,
        inputFormatters: action.inputFormatters ?? this.inputFormatters,
        onEditingComplete: action.onEditingComplete ?? this.onEditingComplete,
        onFieldSubmitted: action.onFieldSubmitted ?? this.onFieldSubmitted,
        onTextChangeListener: action.onTextChangeListener ??
            this.onTextChangeListener
    );
  }
}

class SfwTilBool {
  final bool obscureText;

  final bool maxLengthEnforced;

  final bool expands;

  final bool enabled;
  final bool enableInteractiveSelection;
  final bool hasFloatingPlaceholder;
  final bool isDense;
  final bool filled;
  final bool alignLabelWithHint;
  final bool isOutline;
  final bool autoCorrect;
  final bool autoValidate;
  final bool autoFocus;
  final bool showPasswordToggleIcon;
  final bool enableCounter;
  final bool showCursor;
  final bool enableAutoScroll;
  final bool removeErrorOnTextChanged;
  final bool enableSuggestions;

  const SfwTilBool({this.enableCounter = false,
    this.showPasswordToggleIcon = false,
    this.autoCorrect = true,
    this.autoValidate = false,
    this.autoFocus = false,
    this.obscureText = false,
    this.maxLengthEnforced = true,
    this.expands = false,
    this.enabled = true,
    this.enableInteractiveSelection = true,
    this.hasFloatingPlaceholder = true,
    this.showCursor = true,
    this.enableAutoScroll = true,
    this.isDense,
    this.filled,
    this.alignLabelWithHint,
    this.isOutline = false,
    this.removeErrorOnTextChanged=true,
    this.enableSuggestions = true});

  SfwTilBool copyWith(SfwTilBool booleans) {
    return SfwTilBool(
        enableCounter: booleans.enableCounter ?? this.enableCounter,
        showPasswordToggleIcon: booleans.showPasswordToggleIcon ??
            this.showPasswordToggleIcon,
        autoCorrect: booleans.autoCorrect ?? this.autoCorrect,
        autoValidate: booleans.autoValidate ?? this.autoValidate,
        autoFocus: booleans.autoFocus ?? this.autoFocus,
        obscureText: booleans.obscureText ?? this.obscureText,
        maxLengthEnforced: booleans.maxLengthEnforced ?? this.maxLengthEnforced,
        expands: booleans.expands ?? this.expands,
        enabled: booleans.enabled ?? this.enabled,
        enableInteractiveSelection: booleans.enableInteractiveSelection ??
            this.enableInteractiveSelection,
        hasFloatingPlaceholder: booleans.hasFloatingPlaceholder ??
            this.hasFloatingPlaceholder,
        showCursor: booleans.showCursor ?? this.showCursor,
        enableAutoScroll: booleans.enableAutoScroll ?? this.enableAutoScroll,
        isDense: booleans.isDense ?? this.isDense,
        filled: booleans.filled ?? this.filled,
        alignLabelWithHint: booleans.alignLabelWithHint ??
            this.alignLabelWithHint,
        isOutline: booleans.isOutline ?? this.isOutline,
        removeErrorOnTextChanged: booleans.removeErrorOnTextChanged ??
            this.removeErrorOnTextChanged
    );
  }
}

class SfwTilDecoration {
  final Color fillColor;
  final Color focusColor;
  final Color hoverColor;
  final Color errorBorderColor;
  final Color focusedBorderColor;
  final Color focusedErrorBorderColor;
  final Color disabledBorderColor;
  final Color enabledBorderColor;
  final Color normalBorderColor;

  final InputBorder errorBorder;
  final InputBorder focusedBorder;
  final InputBorder focusedErrorBorder;
  final InputBorder enabledBorder;
  final InputBorder disabledBorder;
  final InputBorder border;
  final EdgeInsets contentPadding;
  final EdgeInsets scrollPadding;
  final BorderRadius outlineBorderRadius;
  final BorderRadius underlineBorderRadius;
  final FloatingLabelBehavior floatingLabelBehavior;
  final BoxConstraints prefixIconConstraints;
  final BoxConstraints suffixIconConstraints;

  const SfwTilDecoration({this.scrollPadding = const EdgeInsets.all(20.0),
    this.fillColor,
    this.focusColor,
    this.hoverColor,
    this.errorBorderColor,
    this.focusedBorderColor,
    this.focusedErrorBorderColor,
    this.disabledBorderColor,
    this.enabledBorderColor,
    this.normalBorderColor,
    this.errorBorder,
    this.focusedBorder,
    this.focusedErrorBorder,
    this.enabledBorder,
    this.disabledBorder,
    this.border,
    this.contentPadding,
    this.outlineBorderRadius,
    this.underlineBorderRadius,
    this.prefixIconConstraints,
    this.suffixIconConstraints,
    this.floatingLabelBehavior = FloatingLabelBehavior.auto});

  SfwTilDecoration copyWith(SfwTilDecoration decoration) {
    return SfwTilDecoration(
        scrollPadding: decoration.scrollPadding ?? this.scrollPadding,
        fillColor: decoration.fillColor ?? this.fillColor,
        focusColor: decoration.focusColor ?? this.focusColor,
        hoverColor: decoration.hoverColor ?? this.hoverColor,
        errorBorderColor: decoration.errorBorderColor ?? this.errorBorderColor,
        focusedBorderColor: decoration.focusedBorderColor ??
            this.focusedBorderColor,
        focusedErrorBorderColor: decoration.focusedErrorBorderColor ??
            this.focusedErrorBorderColor,
        disabledBorderColor: decoration.disabledBorderColor ??
            this.disabledBorderColor,
        enabledBorderColor: decoration.enabledBorderColor ??
            this.enabledBorderColor,
        normalBorderColor: decoration.normalBorderColor ??
            this.normalBorderColor,
        errorBorder: decoration.errorBorder ?? this.errorBorder,
        focusedBorder: decoration.focusedBorder ?? this.focusedBorder,
        focusedErrorBorder: decoration.focusedErrorBorder ??
            this.focusedErrorBorder,
        enabledBorder: decoration.enabledBorder ?? this.enabledBorder,
        disabledBorder: decoration.disabledBorder ?? this.disabledBorder,
        border: decoration.border ?? this.border,
        contentPadding: decoration.contentPadding ?? this.contentPadding,
        outlineBorderRadius: decoration.outlineBorderRadius ??
            this.outlineBorderRadius,
        underlineBorderRadius: decoration.underlineBorderRadius ??
            this.underlineBorderRadius);
  }
}

class SfwTilOthers {
  final ToolbarOptions toolbarOptions;
  final ScrollPhysics scrollPhysics;
  final SmartDashesType smartDashesType;
  final SmartQuotesType smartQuotesType;

  const SfwTilOthers({this.toolbarOptions, this.scrollPhysics, this.smartDashesType, this.smartQuotesType});
}

class TilPassword extends StatefulWidget {
  final SfwTil til;
  final bool isPasswordVisible;
  final KeyValueValidator validator;
  final String validatorKey;

  @override
  State<StatefulWidget> createState() {
    return TilPasswordState();
  }

  TilPassword({
    Key key,
    this.validator,
    this.validatorKey,
    this.til = const SfwTil(),
    this.isPasswordVisible = false,
  });
}

class TilPasswordState extends State<TilPassword> {
  bool _isPasswordVisible = false;
  bool _showError = false;
  bool _isSuffixIconNull = false;
  FocusNode _focusNode = FocusNode();

  //bool _errorShowing = false;
  String _errorText;
  final GlobalKey<SfwTextInputState> inputState = GlobalKey();

  TilPasswordState();

  changeState() {
    setState(() {
      _isPasswordVisible = !_isPasswordVisible;
    });
  }

  @override
  void initState() {
    this._isSuffixIconNull = widget.til.icons.suffixIcon == null ||
        (widget.til.icons.suffixIcon.icon == null &&
            widget.til.icons.suffixIcon.widget == null);
    this._isPasswordVisible = widget.isPasswordVisible ?? false;
    if (widget.til.actions.focusNode != null)
      this._focusNode = widget.til.actions.focusNode;
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        setState(() {
          _showError = false;
        });
      }
    });
    super.initState();
  }

  changeStateOnLongPress(LongPressStartDetails details) {
    changeState();
  }


  @override
  Widget build(BuildContext context) {
    IconData icon;
    this._errorText = widget.til.texts.passwordErrorText != null
        ? widget.til.texts.passwordErrorText
        : SfwStrings.get(SfwStrings.ERR_PASSWORD);

    if (_isSuffixIconNull &&
        widget.til.booleans.showPasswordToggleIcon != null &&
        widget.til.booleans.showPasswordToggleIcon) {
      if (_isPasswordVisible) {
        icon = (widget.til.icons.passwordHiddenIcon == null ||
            widget.til.icons.passwordHiddenIcon.icon == null)
            ? Icons.visibility_off
            : widget.til.icons.passwordHiddenIcon.icon;
      } else {
        icon = (widget.til.icons.passwordShowIcon == null ||
            widget.til.icons.passwordShowIcon.icon == null)
            ? Icons.visibility
            : widget.til.icons.passwordHiddenIcon.icon;
      }
    }

    return SfwTextInput(
      key: inputState,
      validator: widget.validator,
      validatorKey: widget.validatorKey,
      til: SfwTil(
          width: widget.til.width,
          height: widget.til.height,
          alignment: widget.til.alignment,
          isWHpercentage: widget.til.isWHpercentage,
          decoration: widget.til.decoration,
          styles: widget.til.styles,
          textProperty: widget.til.textProperty,
          textsRelated: widget.til.textsRelated,
          icons: (widget.til.booleans.showPasswordToggleIcon != null &&
              widget.til.booleans.showPasswordToggleIcon)
              ? SfwTilIcons(
            counter: widget.til.icons.counter,
            icon: widget.til.icons.icon,
            prefix: widget.til.icons.prefix,
            suffix: widget.til.icons.suffix,
            prefixIcon: widget.til.icons.prefixIcon,
            suffixIcon: icon == null
                ? widget.til.icons.suffixIcon == null
                ? SfwIconData()
                : widget.til.icons.suffixIcon
                : SfwIconData(
                size: widget.til.icons.suffixIcon.size,
                icon: icon,
                tint: widget.til.icons.suffixIcon.tint,
                onPressed: changeState),
          )
              : widget.til.icons,
          texts: SfwTilText(
            text: widget.til.texts.text,
            errorText: _showError == true ? _errorText  : widget.til.texts.errorText,
            hintText: widget.til.texts.hintText,
            helperText: widget.til.texts.helperText,
            labelText: widget.til.texts.labelText ??
                SfwStrings.get(SfwStrings.PASSWORD),
            counterText: widget.til.texts.counterText,
            prefixText: widget.til.texts.prefixText,
            semanticCounterText: widget.til.texts.semanticCounterText,
            suffixText: widget.til.texts.suffixText,
          ),
          booleans: SfwTilBool(
              autoValidate: widget.til.booleans.autoValidate,
              isOutline: widget.til.booleans.isOutline,
              alignLabelWithHint: widget.til.booleans.alignLabelWithHint,
              autoCorrect: widget.til.booleans.autoCorrect,
              autoFocus: widget.til.booleans.autoFocus,
              enableCounter: widget.til.booleans.enableCounter,
              enabled: widget.til.booleans.enabled,
              enableInteractiveSelection:
              widget.til.booleans.enableInteractiveSelection,
              expands: widget.til.booleans.expands,
              filled: widget.til.booleans.filled,
              hasFloatingPlaceholder:
              widget.til.booleans.hasFloatingPlaceholder,
              isDense: widget.til.booleans.isDense,
              maxLengthEnforced: widget.til.booleans.maxLengthEnforced,
              obscureText: !_isPasswordVisible,
              showPasswordToggleIcon:
              widget.til.booleans.showPasswordToggleIcon),
          actions: SfwTilActions(
            controller: widget.til.actions.controller,
            inputFormatters: widget.til.actions.inputFormatters,
            onEditingComplete: widget.til.actions.onEditingComplete,
            onFieldSubmitted: widget.til.actions.onFieldSubmitted,
            onSaved:(val) {
              if(widget.til.actions.onSaved!=null) {
                widget.til.actions.onSaved(val);
              }
            } ,
            validator: widget.til.actions.validator ??
                    (String text) {
                  _showError = !Validator.isValidPassword(text);

                  return  !_showError? null : _errorText;
                },
            focusNode: _focusNode,
          )),
    );
  }
}

class ProgressButton extends StatefulWidget {
  final ProgressButtonModel model;
  final String stateKey;

  @override
  State<StatefulWidget> createState() {
    return _ProgressButtonState();
  }

  /// milliSecondsToNormal -> set -1 to avoid the button state is reset to normal
  ProgressButton(
      this.model, [
        this.stateKey,
      ]) : assert(model != null);
}

enum ProgressButtonStates { normal, success, progress }

class _ProgressButtonState extends SfwState<ProgressButton>
    implements SfwNotifierListener {
  ProgressButtonModel _model;

  _ProgressButtonState();

  @override
  Future<dynamic> onSfwNotifierCalled(String key, data) async {
    if (data is ProgressButtonModel) {
      _model.text = data.text == null ? _model.text : data.text;
      _model.state = data.state == null ? _model.state : data.state;
      setState();
      return true;
    }
    return false;
  }

  @override
  void initState() {
    this._model = widget.model;
    if (_model.successWidget == null) {
      _model.successWidget = SfwIconData(
          widget: Icon(Icons.check,
              size: SfwHelper.setHeight(_model.height == null
                  ? SfwConstants.instance().hBtnCommon
                  : _model.height),
              color: SfwColors.accentColor));
    } else if (_model.successWidget.widget == null) {
      _model.successWidget = SfwIconData(
          widget: Icon(
              _model.successWidget.icon == null
                  ? Icons.check
                  : _model.successWidget.icon,
              size: _model.successWidget.size != null
                  ? _model.successWidget.size
                  : SfwHelper.setHeight(_model.height == null
                  ? SfwConstants.instance().hBtnCommon
                  : _model.height),
              color: _model.successWidget.tint == null
                  ? SfwColors.accentColor
                  : _model.successWidget.tint));
    }

    SfwNotifierForSingleKey.addListener(this, widget.stateKey);

    if (_model.state != ProgressButtonStates.normal &&
        _model.milliSecondsToNormal > -1)
      runTimer(_model.milliSecondsToNormal, ProgressButtonStates.normal);

    super.initState();
  }

  @override
  void dispose() {
    SfwNotifierForSingleKey.removeListener(this, widget.stateKey);
    super.dispose();
  }

  /*@override
  void didUpdateWidget(ProgressButton oldWidget) {
    _model = widget.model;
    super.didUpdateWidget(oldWidget);
  }*/

  void changeState(ProgressButtonStates state) {
    if(!isDestroyed) {
      setState(() {
        _model.state = state;
      });
    }
  }

  void runTimer(int milliSeconds, ProgressButtonStates toState) {
    if (milliSeconds != null && milliSeconds > -1)
      Timer(Duration(milliseconds: milliSeconds), () {
        changeState(toState);
      });
  }

  @override
  Widget build(BuildContext context) {
    switch (_model.state) {
      case ProgressButtonStates.progress:
        return Center(
          child: _model.progressWidget != null
              ? _model.progressWidget
              : CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                _model.progressColor == null
                    ? SfwColors.accentColor
                    : _model.progressColor),
          ),
        );
      case ProgressButtonStates.success:
        return Center(child: _model.successWidget.widget);
      default:
        return SfwUiHelper.button(_model.text, () async {
          setState(() {
            _model.state = ProgressButtonStates.progress;
          });
          bool resolved = await _model.onPressed();
          if (resolved == null || !resolved) {
            runTimer(1000, ProgressButtonStates.normal);
          }
        },
            height: _model.height == null
                ? null
                : SfwHelper.setHeight(_model.height),
            borderRadius: _model.borderRadius,
            backgroundColor: _model.backgroundColor,
            shape: _model.shape,
            splashColor: _model.splashColor,
            widthFactor: _model.widthFactor,
            padding: _model.padding,
            borderColor: _model.borderColor,
            textStyle: _model.textStyle,
            elevation: _model.elevation);
    }
  }
}

class SfwTextInput extends StatefulWidget {
  final SfwTil til;
  final KeyValueValidator validator;
  final String validatorKey;

  const SfwTextInput({Key key, this.til = const SfwTil(),this.validator,this.validatorKey}) : assert((validatorKey==null && validator==null) || (validator!=null && validatorKey!=null) ),   super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SfwTextInputState();
  }
}

class SfwTextInputState extends SfwState<SfwTextInput>
    implements SfwNotifierListener {
  FocusNode _focusNode;
  bool isFocused = false;
  Color tilNormalColor, tilFocusedColor;
  TextEditingController controller;

  bool isTextEmpty = true;
  double _leftPadding = 0;
  double _rightPadding = 0;
  double _iconRightMargin = 0;
  double _iconSize;

  double _iconLeftPadding;

  double _iconRightPadding;

  double _iconBottomPadding;

  double _iconTopPadding;

  double _prefixIconSize;

  double _prefixIconLeftPadding;

  double _prefixIconRightPadding;

  double _prefixIconBottomPadding;

  double _prefixIconTopPadding;

  double _suffixIconSize;

  double _suffixIconLeftPadding;

  double _suffixIconRightPadding;

  double _suffixIconBottomPadding;

  double _suffixIconTopPadding;
  double _height;
  bool isUserDefinedController;

  SfwTil currentTil;
  bool haveValidator;

  @override
  void didUpdateWidget(SfwTextInput oldWidget) {
    super.didUpdateWidget(oldWidget);
    currentTil = widget.til;
    haveValidator = widget.validatorKey!=null && widget.validator !=null;

    if (controller.text != widget.til.texts.text &&
        oldWidget.til.texts.text != widget.til.texts.text) {
      controller.text =
      widget.til.texts.text == null ? "" : widget.til.texts.text;
      controller.selection = TextSelection(
          baseOffset: controller.text == null ? 0.0 : controller.text.length,
          extentOffset: controller.text == null ? 0.0 : controller.text.length);
    }
  }

  @override
  void dispose() {
    try {
      if (widget.til.notifierKey != null)
        SfwNotifierForSingleKey.removeListener(this, widget.til.notifierKey);
      if(!isUserDefinedController || widget.til.disposeUserDefinedController == null || widget.til.disposeUserDefinedController)
        controller.dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Future<dynamic> onSfwNotifierCalled(String key, data) async {
    try {
      controller.text = data == null ? '' : data;
      controller.selection = TextSelection(
          baseOffset: controller.text == null ? 0.0 : controller.text.length,
          extentOffset: controller.text == null ? 0.0 : controller.text.length);
      return true;
    } catch (e) {}
    return false;
  }

  @override
  void initState() {
    currentTil = widget.til;
    haveValidator = widget.validatorKey!=null && widget.validator !=null;
    isUserDefinedController = widget.til.actions.controller != null;
    if (widget.til.notifierKey != null)
      SfwNotifierForSingleKey.addListener(this, widget.til.notifierKey);
    if (widget.til.icons.icon != null ||
        widget.til.icons.prefixIcon != null ||
        widget.til.icons.suffixIcon != null) {
      controller = widget.til.actions.controller == null
          ? TextEditingController()
          : widget.til.actions.controller;
      controller.addListener(() {
        if (!isTextEmpty &&
            (controller.text == null || controller.text.isEmpty)) {
          isTextEmpty = true;

        } else if (isTextEmpty &&
            controller.text != null &&
            controller.text.isNotEmpty) {
          isTextEmpty = false;
        }
        setState();
      });
      controller.text = widget.til.texts.text;
    }
    tilFocusedColor = widget.til.decoration.focusedBorder == null
        ? null
        : widget.til.decoration.focusedBorder.borderSide.color;
    tilNormalColor = widget.til.decoration.border == null
        ? null
        : widget.til.decoration.border.borderSide.color;
    _focusNode = widget.til.actions.focusNode == null
        ? FocusNode()
        : widget.til.actions.focusNode;
    _focusNode.addListener(() {
      setState(() {
        isFocused = _focusNode.hasFocus;
      });
    });
    _iconSize = widget.til.icons.icon.size == null
        ? SfwConstants.instance().edtIconSize
        : widget.til.icons.icon.size;
    _iconLeftPadding = widget.til.icons.icon.iconPadding.left == null
        ? SfwConstants.instance().edtIconPaddingLeft
        : widget.til.icons.icon.iconPadding.left;
    _iconRightPadding = widget.til.icons.icon.iconPadding.right == null
        ? SfwConstants.instance().edtIconPaddingRight
        : widget.til.icons.icon.iconPadding.right;
    _iconBottomPadding = widget.til.icons.icon.iconPadding.bottom == null
        ? 0
        : widget.til.icons.icon.iconPadding.bottom;
    _iconTopPadding = widget.til.icons.icon.iconPadding.top == null
        ? 0
        : widget.til.icons.icon.iconPadding.top;

    _prefixIconSize = widget.til.icons.prefixIcon.size == null
        ? SfwConstants.instance().edtIconSize
        : widget.til.icons.prefixIcon.size;
    _prefixIconLeftPadding =
    widget.til.icons.prefixIcon.iconPadding.left == null
        ? SfwConstants.instance().edtIconPaddingLeft
        : widget.til.icons.prefixIcon.iconPadding.left;
    _prefixIconRightPadding =
    widget.til.icons.prefixIcon.iconPadding.right == null
        ? SfwConstants.instance().edtIconPaddingRight
        : widget.til.icons.prefixIcon.iconPadding.right;
    _prefixIconBottomPadding =
    widget.til.icons.prefixIcon.iconPadding.bottom != null
        ? widget.til.icons.prefixIcon.iconPadding.bottom
        : 0;
    _prefixIconTopPadding = widget.til.icons.prefixIcon.iconPadding.top != null
        ? widget.til.icons.prefixIcon.iconPadding.top
        : 0;

    _suffixIconSize = widget.til.icons.suffixIcon.size == null
        ? SfwConstants.instance().edtIconSize
        : widget.til.icons.suffixIcon.size;
    _suffixIconLeftPadding =
    widget.til.icons.prefixIcon.iconPadding.left == null
        ? SfwConstants.instance().edtIconPaddingLeft
        : widget.til.icons.prefixIcon.iconPadding.left;
    _suffixIconRightPadding =
    widget.til.icons.suffixIcon.iconPadding.right == null
        ? SfwConstants.instance().edtIconPaddingRight
        : widget.til.icons.suffixIcon.iconPadding.right;
    _suffixIconBottomPadding =
    widget.til.icons.suffixIcon.iconPadding.bottom != null
        ? widget.til.icons.suffixIcon.iconPadding.bottom
        : 0;
    _suffixIconTopPadding = widget.til.icons.suffixIcon.iconPadding.top == null
        ? 0
        : widget.til.icons.suffixIcon.iconPadding.top;

    if (widget.til.icons.prefixIcon.icon != null ||
        widget.til.icons.prefixIcon.widget != null) {
      _leftPadding +=
          _prefixIconSize + _prefixIconLeftPadding + _prefixIconRightPadding;
    }

    if (widget.til.icons.suffixIcon.icon != null ||
        widget.til.icons.suffixIcon.widget != null) {
      _rightPadding +=
          _suffixIconSize + _suffixIconLeftPadding + _suffixIconRightPadding;
    }
    if (_leftPadding == 0) {
      _leftPadding = SfwConstants.instance().edtLeftPadding;
    }
    if (_rightPadding == 0) {
      _rightPadding = SfwConstants.instance().edtRightPadding;
    }

    _height = widget.til.booleans.isOutline == null ||
        !widget.til.booleans.isOutline ||
        (widget.til.height != null && widget.til.height > 0)
        ? widget.til.height == null ? 0 : widget.til.height
        : SfwConstants.instance().hEdtCommon;
    if(widget.til.actions.onTextChangeListener!=null || (widget.til.booleans.removeErrorOnTextChanged ?? false)) {
      controller.addListener((){

        if(widget.til.actions.onTextChangeListener!=null) {
          widget.til.actions.onTextChangeListener(
              setState, controller, widget.til);
        } else if(widget.til.booleans.removeErrorOnTextChanged ) {
          bool validatorExits = controller.text != '' && haveValidator && widget.validator.getError(widget.validatorKey)!=null;
          if(controller.text != '' && (validatorExits ||  (currentTil.texts.errorText != null && currentTil.texts.errorText != ''))) {
            setState(() {
              if(validatorExits) {
                widget.validator?.removeError(widget.validatorKey ?? "");
              } else {
                currentTil = currentTil.copyWith(SfwTil(icons:null,styles:null,decoration:null,booleans:null,actions:null,textProperty:null,textsRelated:null,texts: currentTil.texts.copyWith(SfwTilText(errorText: StaticConstants.NULL_REPLACE_TEXT))));
              }

            });
          }
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(currentTil.notifierKey==null || currentTil.notifierKey == '')
      return _buildWidget(context);
    return SfwNotifierSingleKeyWidget(notifierKey: currentTil.notifierKey,builder: (context,key,data){
      if(data is SfwTil) {
        currentTil = data;
      }
      return _buildWidget(context);
    },);
  }


  Widget _buildWidget(BuildContext context) {
    List<Widget> row = [];
    List<Widget> stackChildren = [];
    bool isHavingBottomText = (haveValidator && widget.validator.getError(widget.validatorKey)!=null) || (currentTil.texts.errorText != null &&
        currentTil.texts.errorText.trim().isNotEmpty) ||
        (currentTil.texts.helperText != null &&
            currentTil.texts.helperText.trim().isNotEmpty) ||
        (currentTil.texts.counterText != null &&
            currentTil.texts.counterText.trim().isNotEmpty) || (currentTil.booleans.enableCounter != null &&
        currentTil.booleans.enableCounter) ;
    double _extraBottomPadding = 0;
    if(isHavingBottomText) {
      _extraBottomPadding = SfwHelper.setHeight(70);
    }

    double topBottom = currentTil.isWHpercentage != null &&
        !currentTil.isWHpercentage &&
        _height > 0
        ? _height / 2
        : 0;

    final padding = currentTil.decoration.contentPadding == null
        ? EdgeInsets.only(
        left: _leftPadding,
        right: _rightPadding,
        bottom: currentTil.booleans.isOutline == null ||
            !currentTil.booleans.isOutline
            ? SfwConstants.instance().edtBottomPadding
            : SfwConstants.instance().edtBottomPaddingWhenUsingOutline +
            SfwConstants.instance().edtIconSize -
            SfwHelper.setHeight(20) +
            topBottom,
        top: topBottom)
        : EdgeInsets.only(
        left: currentTil.decoration.contentPadding.left,
        top: currentTil.decoration.contentPadding.top == null
            ? topBottom
            : currentTil.decoration.contentPadding.top + topBottom,
        right: currentTil.decoration.contentPadding.right,
        bottom: currentTil.decoration.contentPadding.bottom == null
            ? topBottom
            : currentTil.decoration.contentPadding.bottom + topBottom);

    if (currentTil.icons.icon.icon != null ||
        currentTil.icons.icon.widget != null) {
      Color iconColor = _focusNode.hasFocus
          ? currentTil.icons.icon.focusTint == null
          ? SfwColors.edtIconFocused == null
          ? tilFocusedColor
          : SfwColors.edtIconFocused
          : currentTil.icons.icon.focusTint
          : currentTil.icons.icon.tint == null
          ? SfwColors.edtIconNormal == null
          ? tilNormalColor
          : SfwColors.edtIconNormal
          : currentTil.icons.icon.tint;
      _iconRightMargin = _iconRightPadding;

      row.add(
//        Expanded(
//          flex: 1,
//          child:
        Padding(
          padding: EdgeInsets.only(
              right: SfwHelper.setWidth(_iconRightPadding),
              left: SfwHelper.setWidth(_iconLeftPadding),
              bottom: _extraBottomPadding
            // bottom: SfwHelper.setHeight(_iconBottomPadding==0?padding.bottom:_iconBottomPadding),
            // top: SfwHelper.setHeight(_iconTopPadding),
          ),
          child: currentTil.icons.icon.widget != null
              ? currentTil.icons.icon.widget
              : Container(
            alignment: FractionalOffset.centerLeft,
            width: _iconSize,
            child: currentTil.icons.icon.onPressed == null
                ? Icon(
              currentTil.icons.icon.icon,
              size: _iconSize,
              color: iconColor,
            )
                : SfwUiHelper.circleWidget(
                Icon(
                  currentTil.icons.icon.icon,
                  size: _iconSize,
                  color: iconColor,
                ),
                _iconSize + SfwHelper.pxToDp(20),
                currentTil.icons.icon.onPressed),
          ),
        ),
//        ),
      );
    }
    Widget textInput = SfwUiHelper.til(
      til: SfwTil(
        textsRelated: currentTil.textsRelated,
        styles: currentTil.styles,
        texts: controller != null
            ? SfwTilText(
            labelText: currentTil.texts.labelText,
            helperText: currentTil.texts.helperText,
            hintText: currentTil.texts.hintText,
            errorText: haveValidator ? widget.validator.getError(widget.validatorKey) : currentTil.texts.errorText,
            text: null,
            counterText: currentTil.texts.counterText,
            suffixText: currentTil.texts.suffixText,
            prefixText: currentTil.texts.prefixText,
            semanticCounterText: currentTil.texts.semanticCounterText)
            : currentTil.texts,
        decoration: currentTil.decoration.copyWith(SfwTilDecoration(
          contentPadding: padding,
        ),),
        booleans: currentTil.booleans,
        actions: currentTil.actions
            .copyWith(SfwTilActions(focusNode: _focusNode, controller: controller,onSaved: widget.validator==null?null:(val){
          widget.validator.addValue(widget.validatorKey, val);
          if(currentTil.actions.onSaved!=null) {
            currentTil.actions.onSaved(val);
          }

        }),),
        textProperty: currentTil.textProperty,
        icons: SfwTilIcons(counter: currentTil.icons.counter),
      ),
    );
//    if(_currentTil.booleans.enableAutoScroll && !_currentTil.booleans.enabled) {
//      textInput = SingleChildScrollView(child: textInput,scrollDirection: Axis.horizontal,);
//    }

    stackChildren.add(
        Container(
//        width: _currentTil.isWHpercentage != null && !_currentTil.isWHpercentage
//            ? _currentTil.width
//            : null,
//        height: _currentTil.isWHpercentage != null && !_currentTil.isWHpercentage
//            ? _currentTil.height
//            : null,
          margin: EdgeInsets.only(
              left: SfwHelper.setWidth(
                  _iconRightMargin == null || _iconRightMargin < 0
                      ? 0
                      : _iconRightMargin)),
          child: textInput,
        )
    );
    if (currentTil.icons.prefixIcon.icon != null ||
        currentTil.icons.prefixIcon.widget != null) {
      Color iconColor = _focusNode.hasFocus
          ? currentTil.icons.prefixIcon.focusTint == null
          ? SfwColors.edtIconPrefixFocused == null
          ? tilFocusedColor
          : SfwColors.edtIconPrefixFocused
          : currentTil.icons.prefixIcon.focusTint
          : currentTil.icons.prefixIcon.tint == null
          ? SfwColors.edtIconPrefixNormal
          : currentTil.icons.prefixIcon.tint;
      stackChildren.add(
        Container(
          alignment: isTextEmpty
              ? FractionalOffset.centerLeft
              : FractionalOffset.centerLeft,
//          margin: EdgeInsets.only(
//            left: SfwHelper.setWidth(_iconRightMargin),
//          ),
          width: _prefixIconSize,
          child: Padding(
              padding: EdgeInsets.only(
                right: _prefixIconRightPadding == null
                    ? SfwConstants.instance().edtIconPaddingRight
                    : _prefixIconRightPadding,
                left: _prefixIconLeftPadding == null
                    ? SfwConstants.instance().edtIconPaddingLeft
                    : _prefixIconLeftPadding,
                bottom: _prefixIconBottomPadding+_extraBottomPadding,
                top: _prefixIconTopPadding,
              ),
              child: currentTil.icons.prefixIcon.widget != null
                  ? currentTil.icons.prefixIcon.widget
                  : currentTil.icons.prefixIcon.onPressed == null
                  ? Icon(
                currentTil.icons.prefixIcon.icon,
                size: _prefixIconSize,
                color: iconColor,
              )
                  : SfwUiHelper.circleWidget(
                  Icon(
                    currentTil.icons.prefixIcon.icon,
                    size: _prefixIconSize,
                    color: iconColor,
                  ),
                  _prefixIconSize + SfwHelper.pxToDp(20),
                  currentTil.icons.prefixIcon.onPressed)),
        ),
      );
    }
    if (currentTil.icons.suffixIcon.icon != null ||
        currentTil.icons.suffixIcon.widget != null) {
      Color iconColor = _focusNode.hasFocus
          ? currentTil.icons.suffixIcon.focusTint != null
          ? currentTil.icons.suffixIcon.focusTint
          : SfwColors.edtIconSuffixFocused == null
          ? tilFocusedColor
          : SfwColors.edtIconSuffixFocused
          : currentTil.icons.suffixIcon.tint != null
          ? currentTil.icons.suffixIcon.tint
          : SfwColors.edtIconSuffixNormal;
      stackChildren.add(
        Padding(
          padding: EdgeInsets.only(
            right: _suffixIconRightPadding == null
                ? SfwConstants.instance().edtIconPaddingRight
                : _suffixIconRightPadding,
            left: _suffixIconLeftPadding == null
                ? SfwConstants.instance().edtIconPaddingLeft
                : _suffixIconLeftPadding,
            bottom: _suffixIconBottomPadding+_extraBottomPadding,
            top: _suffixIconTopPadding,
          ),
          child: Align(
            alignment: FractionalOffset.centerRight,
            child: currentTil.icons.suffixIcon.widget != null
                ? currentTil.icons.suffixIcon.widget
                : currentTil.icons.suffixIcon.onPressed != null
                ? SfwUiHelper.circleWidget(
                Icon(
                  currentTil.icons.suffixIcon.icon,
                  color: iconColor,
                  size: _suffixIconSize,
                ),
                _suffixIconSize + SfwHelper.pxToDp(20),
                currentTil.icons.suffixIcon.onPressed)
                : Icon(
              currentTil.icons.suffixIcon.icon,
              color: iconColor,
              size: _suffixIconSize,
            ),
          ),
        ),
      );
    }
    row.add(Expanded(
//        flex: 8,
        child: Stack(
          alignment: isTextEmpty && !_focusNode.hasFocus
              ? FractionalOffset.centerLeft
              : FractionalOffset.centerLeft,
          children: stackChildren,
        )));

    Widget finalWidget = Row(
      mainAxisSize: MainAxisSize.max,
      children: row,
    );

    if ((currentTil.isWHpercentage == null || !currentTil.isWHpercentage) &&
        (currentTil.height != null || currentTil.width != null)) {
      if (currentTil.height != null && currentTil.width != null) {
        finalWidget = SizedBox(
          width: currentTil.width,
          height: currentTil.height,
          child: finalWidget,
        );
      } else if (currentTil.height == null && currentTil.width != null) {
        finalWidget = SizedBox(
          width: currentTil.width,
          child: finalWidget,
        );
      } else if (currentTil.height != null && currentTil.width == null) {
        finalWidget = SizedBox(
          height: currentTil.height,
          child: finalWidget,
        );
      }
    }
    if (currentTil.alignment != null &&
        currentTil.isWHpercentage != null &&
        currentTil.isWHpercentage) {
      finalWidget = FractionallySizedBox(
        alignment: currentTil.alignment,
        child: finalWidget,
        heightFactor: currentTil.height,
        widthFactor: currentTil.width,
      );
    } else if (currentTil.alignment != null) {
      finalWidget = Align(
        alignment: currentTil.alignment,
        child: finalWidget,
      );
    }

    return finalWidget;
  }
}

class SfwCheckBox extends StatefulWidget {
  final bool isChecked;
  final ValueChanged<bool> onChanged;
  final Color normalColor;
  final Color checkedColor;
  final MaterialTapTargetSize materialTapTargetSize;
  final IconData checkedIcon;
  final IconData normalIcon;
  final Widget text;
  final AlignmentGeometry alignment;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double iconSize;
  final double paddingWithText;
  final double width;
  final double height;
  final bool isRadio;
  final int id;

//  /// You can use this in a group of checkboxes
//  /// step 1:  create a bloc object
//  /// step 2:  pass the same bloc object in all checkboxes
//  /// step 3:  pass a unique id to all checkbox
//  /// step 4:  set isForUniqueGroup = true
//  ///
//  /// Also you can use the bloc object to change the text and checked status value  -> Note : Don't use same bloc object to change the status and text
//  final SfwCheckboxBloc bloc;

  const SfwCheckBox(
      {Key key,
        this.alignment,
        this.width,
        this.height,
        this.padding,
        this.margin,
        this.iconSize,
        this.isChecked = false,
        @required this.onChanged,
        this.normalColor,
        this.checkedColor,
        this.materialTapTargetSize,
        this.checkedIcon,
        this.normalIcon,
        this.text,
        this.paddingWithText,
        this.isRadio = false,
        this.id})
      : assert(isChecked != null),
        assert(isRadio != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    IconData cIcon = this.checkedIcon == null
        ? this.isRadio ? Icons.check_circle : Icons.check_box
        : this.checkedIcon;
    IconData nIcon = this.normalIcon == null
        ? this.isRadio
        ? Icons.check_circle_outline
        : Icons.check_box_outline_blank
        : this.normalIcon;

    return _SfwCheckBoxState(
        this.isChecked != null && this.isChecked, cIcon, nIcon);
  }
}

class _SfwCheckBoxState extends State<SfwCheckBox> {
  bool _isChecked = false;
  final IconData checkedIcon;
  final IconData normalIcon;

  _SfwCheckBoxState(this._isChecked, this.checkedIcon, this.normalIcon);

  @override
  void didUpdateWidget(SfwCheckBox oldWidget) {
    _isChecked = widget.isRadio ? widget.isChecked : _isChecked;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor:
      widget.checkedColor ?? widget.normalColor ?? SfwColors.primaryColor,
      onTap: () {
//        if (widget.isRadio && _isChecked) {
//          return;
//        }
        setState(() {
          _isChecked = !_isChecked;
          if (widget.onChanged != null) {
            widget.onChanged(_isChecked);
          }
        });
      },
      child: Container(
        width: widget.width,
        color: Colors.transparent,
        height: widget.height,
        alignment: widget.alignment ?? FractionalOffset.centerLeft,
        padding: widget.padding,
        margin: widget.margin,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              _isChecked ? checkedIcon : normalIcon,
              color: _isChecked
                  ? widget.checkedColor ?? SfwColors.cbTintChecked
                  : widget.normalColor ?? SfwColors.cbTintNormal,
              size: SfwHelper.pxToDp(
                  widget.iconSize ?? SfwConstants.instance().cbIconSize),
            ),
            widget.text == null
                ? Text("")
                : Padding(
              padding: EdgeInsets.only(
                  left: SfwHelper.setWidth(widget.paddingWithText ??
                      SfwConstants.instance().cbTextLeftMargin)),
              child: widget.text,
            )
          ],
        ),
      ),
    );
  }
}

class AppUi {
  static Widget clickableTil(SfwTextInput input, VoidCallback onTap) {
    return InkWell(
      child: input,
      onTap: onTap,
    );
  }
DATE_STARTED
  static pickDob(
      BuildContext context, String selectedDate, Function(DateTime) onConfirm,
      {locale: LocaleType.en, showTitleActions: true}) {
    pickDate(context, selectedDate, onConfirm,
        showTitleActions: showTitleActions,
        locale: locale,
        minTime: SfwHelper.parse('yyyy-MM-dd', "1900-01-01"),
        maxTime: DateTime.now());
  }

  static pickDate(
      BuildContext context, String selectedDate, Function(DateTime) onConfirm,
      {locale: LocaleType.en,
        showTitleActions: true,
        DateTime minTime,
        DateTime maxTime}) {
    DatePicker.showDatePicker(context,
        showTitleActions: showTitleActions,
        onConfirm: onConfirm,
        maxTime: maxTime,
        minTime: minTime,
        currentTime: SfwHelper.parse(SfwStrings.WEB_DATE_FORMAT, selectedDate,
            defDateTime: DateTime.now()),
        locale: locale);
  }
  DATE_END
  static showDropDown<T>(BuildContext context, Widget child, List<T> list,
      Function(int, T) onSelected) {
    List<PopupMenuEntry<int>> menu = [];
    for (int i = 0; i < list.length; ++i) {
      T item = list[i];

      menu.add(PopupMenuItem(
        child: SfwUiHelper.commonText(
          item is String?item: item is DropdownItemDelegate?item.getDropdownItemString():item.toString(),
        ),
        value: i,
      ));
    }
    return PopupMenuButton(
      itemBuilder: (context) {
        return menu;
      },
      child: child,
      onCanceled: () {
        SfwHelper.hideKeyboard(context);
      },
      onSelected: (val) {
        SfwHelper.hideKeyboard(context);
        if (onSelected != null) onSelected(val, list[val]);
      },
    );
  }

  static SfwTextInput input(String text, String label, Function(String) onSaved,
      {TextInputType inputType = TextInputType.text,
        String notifierKey,
        TextInputAction inputAction: TextInputAction.next,
        int maxLines,
        int minLines,
        int maxLength,
        int minLength,
        String error,
        bool isEnabled = true,
        bool isOutline = true,
        List<TextInputFormatter> inputFormatters,
        SfwTextChangeCallBack callBack,
        TextEditingController controller,
        bool disposeUserDefinedController = true,
        FocusNode focusNode,
        SfwTilIcons icons = const SfwTilIcons()}) {
    return SfwTextInput(
      til: SfwTil(
          disposeUserDefinedController:disposeUserDefinedController,
          notifierKey: notifierKey,
          texts: SfwTilText(
            labelText: label,
            text: text,
            errorText: error,
          ),
          icons: icons,
          actions: SfwTilActions(
            onTextChangeListener: callBack,
            onSaved: onSaved,
            controller: controller,
            focusNode: focusNode,
            inputFormatters: inputFormatters,
          ),
          booleans: SfwTilBool(
            isOutline: isOutline,
            enabled: isEnabled,
          ),
          textProperty: SfwTilTextProperty(
            inputType: inputType,
            inputAction: inputAction,
          ),
          textsRelated: SfwTilTextsRelated(
            maxLines: maxLines,
            minLines: minLines,
            maxLength: maxLength,
            minLength: minLength,
          )),
    );
  }
}
