import 'package:build/build.dart';
import 'package:sfw_imports/sfw_imports.dart';
import 'package:source_gen/source_gen.dart';
import 'entity.dart' as entityGenerator;
import 'output_helpers.dart';

class GeneratorHelper {
  SfwConfig configuration;

  readSfwConfig(BuildStep buildStep) async {
    String config = await readAsset(
        AssetId(buildStep.inputId.package, "lib/sfw/sfw.dart"), buildStep);
    config = config.replaceAll(" ", '');

    int totalFileCount =
        int.parse(readValue("TOTAL_DART_FILE_COUNT", config, "0"));
    bool debuggingEnabled =
        bool.fromEnvironment(readValue("DEBUGGING_ENABLED", config, "false"));
    SfwDbConfig dbConfig = SfwDbConfig(readValue("DB_DART_FILE_PATH", config),
        version: int.parse(readValue("DB_VERSION_CODE", config, "1")));
    SfwImportLibraryConfig importLibraryConfig = SfwImportLibraryConfig(
        includeCachedNetworkImage:
            config.contains("INCLUDE_CACHED_NETWORK_IMAGE=true"),
        includeDio: config.contains("INCLUDE_DIO=true"),
        includeFlutterDatetimePicker:
            config.contains("INCLUDE_FLUTTER_DATE_TIME_PICKER=true"),
        includeFlutterSpinKit: config.contains("INCLUDE_FLUTTER_SPIN_KIT=true"),
        includeFlutterToast: config.contains("INCLUDE_FLUTTER_TOAST=true"),
        includePermissionHandler:
            config.contains("INCLUDE_PERMISSION_HANDLER=true"));
    configuration = SfwConfig(
        importLibraryConfig: importLibraryConfig,
        dbConfig: dbConfig,
        totalFileCount: totalFileCount,
        debuggingEnabled: debuggingEnabled);
  }

  String readValue(String key, String content, [String defaultValue = ""]) {
    int index = content.indexOf(key);
    if (index > -1) {
      content = content.substring(index + key.length + 1);
      index = content.indexOf(";");
      if (index > -1) {
        return content.substring(0, index).replaceAll('"', '');
      }
    }
    return defaultValue;
  }

  Future<String> readAsset(AssetId assetId, BuildStep buildStep) async {
    try {
      return await buildStep.readAsString(assetId);
    } catch (e) {
      return e.toString();
    }
  }

  entityReader(LibraryReader library, BuildStep buildStep) async {
    entityGenerator.EntitiesGenerator entities =
        entityGenerator.EntitiesGenerator();
    for (var annotatedElement
        in library.annotatedWith(TypeChecker.fromRuntime(SfwEntity))) {
      entities.generateForAnnotatedElement(library.element,
          annotatedElement.element, annotatedElement.annotation, buildStep);
    }
    if (entityGenerator.error == null) {
      final values = Set<String>();
      StringBuffer s = StringBuffer();
      if (entityGenerator.error == null) {
        for (var import in entityGenerator.imports) {
          s.write(import);
        }
      } else {
        s.writeln(entityGenerator.error);
      }

      await for (var value in normalizeGeneratorOutput(s.toString())) {
        assert(value == null || (value.length == value.trim().length));
        values.add(value);
      }
    }
  }
}
