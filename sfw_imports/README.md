# sfw_imports

* Import files for [sfw_generator](https://pub.dev/packages/sfw_generator)




## examples

    @SfwEntity(["tbl_logged_user","tbl_users"],needTableMethods : true,isAnDbEntity = true,setCopyWithFunction = true)
    class UserModel with HelperMethods {
       @SfwDbPrimary(true)
      int id;//Can use String also.  Autoincrement does not run on String type
      @SfwDbField("email_primary")
      String email;

      String firstName;
      String lastName;

      @SfwDbExclude(tables : ["tbl_users"])
      String countryCode;
      String mobile;

      String token;
      String image;
      @SfwDbField("created_at")
      String createdAt;
      @SfwDbField("updated_at")
      String updatedAt;

      @SfwDbExclude()
      bool keepSignedIn;

      UserModel(
          {this.id,
            this.email,
            this.firstName,
            this.lastName,
            this.countryCode,
            this.mobile,
            this.token,
            this.image,
            this.createdAt,
            this.updatedAt,
            this.keepSignedIn});

    }