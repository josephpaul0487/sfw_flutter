import 'dart:async' show FutureOr;

typedef OnClickCallback = void Function();
typedef OnClickCallbackWithTag = void Function(Object tag);
typedef OnClickCallbackWithTag2 = FutureOr<bool> Function(Object tag);
typedef OnClickCallback2 = FutureOr<bool> Function();

/// This interface can be used to avoid multiple click
class OnClickListener {
  bool clicked = false;
  final Object tag;
  final Function() listener;
  final OnClickCallbackWithTag listenerWithTag;

  OnClickListener({this.listener, this.listenerWithTag, this.tag});

  void onClick() {
    if (clicked) return;
    clicked = true;
    if (this.listener != null) this.listener();
    if (this.listenerWithTag != null) this.listenerWithTag(tag);
    clicked = false;
  }
}

/// Same as [OnClickListener] except the callback listener return value
/// [OnClickListener] callback function does not have a return value
class OnClickListener2 {
  bool clicked = false;
  final Object tag;
  final OnClickCallback2 listener;
  final OnClickCallbackWithTag2 listenerWithTag;

  OnClickListener2({this.listener, this.listenerWithTag, this.tag});

  void onClick() {
    if (clicked) return;
    clicked = true;
    if (this.listener != null) this.listener();
    if (this.listenerWithTag != null) this.listenerWithTag(tag);
    clicked = false;
  }
}

typedef BoolCallBack = bool Function();
