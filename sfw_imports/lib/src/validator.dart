import 'dart:core';

/// Use this class to validate password,number,name ,email and special characters
/// All string parameters changed to dynamic to avoid errors in KeyValueValidator
class Validator {
  static const int passwordMin = 6;
  static const int passwordMax = 32;
  static final RegExp passwordPattern = RegExp(
      '(((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#&%]).{' +
          '$passwordMin,$passwordMax}))');
  static final RegExp numberPattern = RegExp(r'\d');
  static final RegExp namePattern = RegExp(r"[a-zA-Z0-9 ]*");
  static final RegExp specialCharactersPattern = RegExp(r"[$=\\\\|<>^*%]");
  static final RegExp emailPattern = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  static bool isValidPassword(dynamic password) {
//    return password?.contains(passwordPattern, 0);
    return password != null && password is String && passwordPattern.hasMatch(password);
  }

  static bool isNameValid(dynamic name) {
    return name != null && name is String && namePattern.hasMatch(name);
  }

  static bool isEmailValid(dynamic email) {
    return email != null  && email is String && emailPattern.hasMatch(email);
  }

  static bool haveMinLength(dynamic str,int minLength) {
    return (str is String || str is Iterable || str is Map) && (str?.length ?? 0)>=(minLength ?? 1);
  }

  static bool checkLengthExceeds(dynamic str,int maxLength) {
    return (str is String || str is Iterable || str is Map) && (str?.length ?? 0)>(maxLength ?? 0);
  }

  static bool haveMinValue(dynamic str,double minValue ) {
    if(!(str is  String) || str==null) {
      return false;
    }
    double value = double.tryParse(str);
    return !(value==null || minValue ==null || value<minValue);
  }
  static bool checkValueExceeds(dynamic str,double maxValue , [String errorMessage]) {
    if(!(str is  String) || str==null) {
      return false;
    }
    double value = double.tryParse(str);
    return  value==null || maxValue ==null || value>maxValue;
  }

  static bool isNumber(dynamic str) {
    if(!(str is  String) || str==null) {
      return false;
    }
    return double.tryParse(str) != null;
  }

  static bool isDouble(dynamic str) {
    if(!(str is  String) || str==null) {
      return false;
    }
    return double.tryParse(str)!=null;
  }

  static bool isInt(dynamic str) {
    if(!(str is  String) || str==null) {
      return false;
    }
    return int.tryParse(str)!=null;
  }
}

/// Errors validator
class KeyValueValidator {
  Map<String, dynamic> _values = {};
  Map<String, String> _errors = {};

  addValue(String key, dynamic value) {
    if (key != null) _values[key] = value;
  }

  addValues(Map<String, dynamic> keyValues) {
    if (keyValues != null) {
      keyValues.forEach((key, value) {
        if (value == null) {
          addValue(key, value);
        } else {
          removeValue(key);
        }
      });
    }
  }

  addError(String key, String error) {
    if (key != null) _errors[key] = error;
  }

  addErrors(Map<String, String> keyValues) {
    if (keyValues != null) _errors.addAll(keyValues);
  }

  removeValue(String key) {
    if (key != null) _values.remove(key);
  }

  removeError(String key) {
    if (key != null) _errors.remove(key);
  }

  clearValues() {
    _values.clear();
  }

  clearErrors() {
    _errors.clear();
  }

  bool containsValue(String key) {
    return _values[key] != null;
  }

  bool containsError(String key) {
    return _errors[key] !=null;
  }

  getValues() {
    return _values.map((key, value) => MapEntry(key, value));
  }

  getErrors() {
    return _errors.map((key, value) => MapEntry(key, value));
  }

  dynamic getValue(String key) {
    if (key == null) return null;

    return _values[key];
  }

  String getError(String key) {
    if (key == null) return null;
    return _errors[key];
  }

  int errorsLength() {
    return _errors.length;
  }

  int valuesLength() {
    return _values.length;
  }

  bool isEmpty(dynamic str) {
    return str == null ||  !(str is String) || str.trim().isEmpty;
  }

  bool checkEmpty(List<String> keys, [String errorMsg = "Required field"]) {
    bool empty = false;
    if (keys != null) {
      keys.forEach((key) {
        if (isEmpty(getValue(key))) {
          empty = true;
          addError(key, errorMsg ?? "Required field");
        }
      });
    }
    return empty;
  }

  bool checkEmail(String key, [String errorMsg = "Invalid email"]) {
    if (!Validator.isEmailValid(getValue(key))) {
      addError(key, errorMsg ?? "Invalid email");
      return false;
    }
    return true;
  }

  bool checkName(String key, [String errorMessage = "Invalid name"]) {
    if (!Validator.isNameValid(getValue(key))) {
      addError(key, errorMessage ?? "Invalid name");
      return false;
    }
    return true;
  }

  bool checkPassword(String key, [String errorMessage = "Invalid password"]) {
    if (!Validator.isValidPassword(getValue(key))) {
      addError(key, errorMessage ?? "Invalid password");
      return false;
    }
    return true;
  }

  bool haveMinLength(String key,int minLength , [String errorMessage]) {
    if (!Validator.haveMinLength(getValue(key),minLength)) {
      addError(key, errorMessage ?? "Minimum length is ${minLength ?? 1}");
      return false;
    }
    return true;
  }
  bool checkLengthExceeds(String key,int maxLength , [String errorMessage]) {
    if (Validator.checkLengthExceeds(getValue(key),maxLength)) {
      addError(key, errorMessage ?? "Maximum length is ${maxLength ?? 1}");
      return true;
    }
    return false;
  }

  bool checkLength(String key,int minLength,int maxLength ,  [String minLengthErrorMessage,String maxLengthErrorMessage]) {
    return haveMinLength(key,minLength,minLengthErrorMessage) && !checkLengthExceeds(key,maxLength,maxLengthErrorMessage);
  }

  bool haveMinValue(String key,double minValue , [String errorMessage]) {
    if (!Validator.haveMinValue(getValue(key), minValue)) {
      addError(key, errorMessage ?? "Minimum value is $minValue");
      return false;
    }
    return true;
  }
  bool checkValueExceeds(String key,double maxValue , [String errorMessage]) {
    if (Validator.checkValueExceeds(getValue(key), maxValue)) {
      addError(key, errorMessage ?? "Maximum value is $maxValue");
      return true;
    }
    return false;
  }

  bool checkValue(String key,double minValue,double maxValue ,  [String minValueErrorMessage,String maxValueErrorMessage]) {
    return haveMinValue(key,minValue,minValueErrorMessage) && !checkValueExceeds(key,maxValue,maxValueErrorMessage);
  }

  bool isNumber(String key,[String errorMessage = "Not a number"]) {
    if (Validator.isNumber(getValue(key))) {
      addError(key, errorMessage ?? "Not a number");
      return false;
    }
    return true;
  }

  bool isDouble(String key,[String errorMessage = "Not a decimal number"]) {
    if (!Validator.isDouble(getValue(key))) {
      addError(key, errorMessage ?? "Not a decimal number");
      return false;
    }
    return true;
  }

  bool isInt(String key,[String errorMessage = "Not a number"]) {
    if (!Validator.isInt(getValue(key))) {
      addError(key, errorMessage ?? "Not a number");
      return false;
    }
    return true;
  }
}
