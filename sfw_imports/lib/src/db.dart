/// The database configuration
/// [dbName]              -> mandatory
/// [version]             -> version code of the db
/// [totalFileCount]      -> Total dart files count of your project except generated files
class SfwDbConfig {
  final String dbName;
  final int version;

  const SfwDbConfig(this.dbName, {this.version = 1})
      : assert(dbName != null && dbName != ""),
        assert(version != null && version > 0);
}

/// Annotate every entity models using this class
/// Every model should have a primary field which annotate with [SfwDbPrimary]
/// [tables]              -> You can define one or more table for a model.  The model name will used as table name if the tables is empty
/// [needTableMethods]    -> true :    Will include all db related queries in the model's SFW class. ie, getSingle,getAll,insertSingle.....
/// [isAnDbEntity]        -> true :    Will create tables in db.
///                        false:    Only create toJson fromJson helper functions
/// [setCopyWithFunction] -> true: Will include copyWith(otherModel) function
class SfwEntity {
  final List<String> tables;
  final bool needTableMethods;
  final bool isAnDbEntity;
  final bool setCopyWithFunction;

  const SfwEntity(this.tables,
      {this.needTableMethods = false,
      this.isAnDbEntity = true,
      this.setCopyWithFunction = false})
      : assert(tables != null && needTableMethods != null);
}

/// Annotate [SfwEntity] model's fields  using this
/// [name]                 -> not mandatory. the field name is the default name for db column name
/// [isAnEntity]           -> Set as true if the field type is an [SfwEntity]
/// [genericType]          -> Set the generic type class if the field is a List
/// [isUnique]             -> true :    Will set as unique in db
/// [canNull]              -> Is the field is nullable
class SfwDbField {
  final String name;
  final bool isAnEntity;
  final Type genericType;
  final bool isUnique;
  final bool canNull;

  const SfwDbField(this.name,
      {this.isAnEntity = false,
      this.genericType,
      this.isUnique = false,
      this.canNull = true})
      : assert(isAnEntity != null),
        assert(isUnique != null),
        assert(canNull != null);
}

/// Annotate [SfwEntity] model's primary field  using this
/// [isAutoIncrement] -> true  : the field should be int type.
class SfwDbPrimary {
  final bool isAutoIncrement;
  const SfwDbPrimary(this.isAutoIncrement) : assert(isAutoIncrement != null);
}

/// Annotate [SfwEntity] model's field  using this to exclude the field from db
/// [tables]    -> If empty :     All tables of the corresponding model will exclude this field
///               ELSE    :     The selected tables will exclude this field
class SfwDbExclude {
  final List<String> tables;

  const SfwDbExclude({this.tables: const []}) : assert(tables != null);
}

/// Use the class where [SfwDbConfig] is defined
/// [entityType]    ->  return type (Should annotate with [SfwEntity])  : Use the EntityModel type if the return type is List
/// [isList]        ->  true :  If the return type is a list
/// [where]         ->  condition
/// [whereArgs]     ->  condition arguments
/// [limit]         ->  provide number if have limit
/// [offset]        ->  used along with limit
/// [columns]       ->  column names
/// Balance all same as sqlite query
/// eg:       @SfwDbQuery(UserModel,false,'tbl_user',limit: 1,orderBy: "updated_at")
///           getUser();
class SfwDbQuery {
  final Type entityType;
  final bool isList;
  final String table;
  final String where;
  final List<dynamic> whereArgs;
  final int limit;
  final bool distinct;
  final List<String> columns;
  final String groupBy;
  final String having;
  final String orderBy;
  final int offset;

  const SfwDbQuery(this.entityType, this.isList, this.table,
      {this.where,
      this.whereArgs,
      this.limit,
      this.distinct,
      this.columns,
      this.groupBy,
      this.having,
      this.orderBy,
      this.offset})
      : assert(entityType != null),
        assert(table != null && table != '');
}
