/// Is used for web api response. See [StatusModel]
mixin StatusInterface {
  bool isSuccess();
  String getMessage();
  Object getTag();
  String getPath();
  setPath(String path);
  setMessage(String msg);
  setTag(Object tag);
  setAsNetworkError(bool isNetworkError);
  String getError();
}

/// Is used for web api response. See [CommonModel]
mixin DataInterface<T> implements StatusInterface {
  T getData();
  setData(T data);
}

/// Is used for web api response. See [CommonModelList]
mixin DataListInterface<T> implements StatusInterface {
  List<T> getData();
  setData(List<T> data);
}

/// helper functions
mixin HelperMethods {
  String avoidNull(String str) {
    return str == null ? "" : str;
  }
}

/// Is used for web api response.
class StatusModel with HelperMethods implements StatusInterface {
  int status;
  int tagCode;
  String msg;
  bool isNetworkError;
  String imagePath;
  List<dynamic> errors;
  Map<String, dynamic> keyValues;

  StatusModel(
      {this.status = 0,
      this.tagCode = 0,
      this.msg = "",
      this.isNetworkError = false,
      this.imagePath = "",
      this.errors,
      this.keyValues});

  @override
  bool isSuccess() {
    return this.status == 1;
  }

  @override
  String getMessage() {
    return avoidNull(msg);
  }

  @override
  setMessage(String msg) {
    this.msg = msg;
  }

  Object getTag() {
    return tagCode;
  }

  @override
  setTag(Object tag) {
    this.tagCode = tag;
  }

  @override
  String getPath() {
    return avoidNull(imagePath);
  }

  @override
  setPath(String path) {
    this.imagePath = path;
  }

  @override
  setAsNetworkError(bool isNetworkError) {
    this.isNetworkError = isNetworkError;
  }

  @override
  String getError() {
    return msg != null && msg != ''
        ? msg
        : errors != null && errors.isNotEmpty ? errors[0] : '';
  }

  factory StatusModel.fromJson(Map<String, dynamic> json) => StatusModel(
      status: json['status'] as int,
      tagCode: json['tagCode'] as int,
      msg: json['msg'] as String,
      imagePath: json['imagePath'] as String,
      errors: json['errors'],
      keyValues: json['keyValues']);
}

/// Is used for web api response.
class CommonModel<T> extends StatusModel implements DataInterface {
  T data;

  CommonModel(
      {this.data,
      int status,
      int tagCode,
      String msg,
      bool isNetworkError = false,
      String imagePath = "",
      List<dynamic> errors,
      Map<String, dynamic> keyValues})
      : super(
            status: status,
            tagCode: tagCode,
            msg: msg,
            isNetworkError: isNetworkError,
            imagePath: imagePath,
            errors: errors);

  factory CommonModel.fromJson(Map<String, dynamic> json) => CommonModel<T>(
      status: json['status'] as int,
      tagCode: json['tagCode'] as int,
      msg: json['msg'] as String,
      imagePath: json['imagePath'] as String,
      errors: json['errors'],
      keyValues: json['keyValues']);

  @override
  getData() {
    return data;
  }

  @override
  setData(data) {
    this.data = data;
  }
}

/// Is used for web api response.
class CommonModelList<T> extends StatusModel implements DataListInterface {
  List<T> data;

  CommonModelList(
      {this.data,
      int status,
      int tagCode,
      String msg,
      bool isNetworkError = false,
      String imagePath,
      List<dynamic> errors,
      Map<String, dynamic> keyValues})
      : super(
            status: status,
            tagCode: tagCode,
            msg: msg,
            isNetworkError: isNetworkError,
            imagePath: imagePath,
            errors: errors);

  factory CommonModelList.fromJson(Map<String, dynamic> json) =>
      CommonModelList<T>(
          status: json['status'] as int,
          tagCode: json['tagCode'] as int,
          msg: json['msg'] as String,
          imagePath: json['imagePath'] as String,
          errors: json['errors'],
          keyValues: json['keyValues']);

  @override
  List getData() {
    return data;
  }

  @override
  setData(List data) {
    this.data = data;
  }
}
