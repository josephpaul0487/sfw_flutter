import 'package:sfw_imports/sfw_imports.dart';

/// Annotate any one class using this
/// This will removed the excluded library features from the generated file. See  [SfwImportLibraryConfig]
class SfwConfig {
  final SfwImportLibraryConfig importLibraryConfig;
  final SfwDbConfig dbConfig;
  final SfwWebConfig webConfig;
  final int totalFileCount;
  final bool debuggingEnabled;
  const SfwConfig(
      {this.importLibraryConfig = const SfwImportLibraryConfig(),
      this.dbConfig = const SfwDbConfig("db"),
      this.totalFileCount,
      this.debuggingEnabled = false,
      this.webConfig = const SfwWebConfig("https://www.google.com/")});
}

/// This will removed the excluded library features from the generated file. See  [SfwConfig.importLibraryConfig]
class SfwImportLibraryConfig {
  final bool includeCachedNetworkImage;
  final bool includeFlutterDatetimePicker;
  final bool includePermissionHandler;
  final bool includeFlutterSpinKit;
  final bool includeFlutterToast;
  final bool includeDio;

  const SfwImportLibraryConfig(
      {this.includeCachedNetworkImage = true,
      this.includeFlutterDatetimePicker = true,
      this.includePermissionHandler = true,
      this.includeFlutterSpinKit = true,
      this.includeFlutterToast = true,
      this.includeDio = true});
}
